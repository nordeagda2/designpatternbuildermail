package Mail;

/**
 * Created by RENT on 2017-08-08.
 */
public enum Type {
    UNKNOWN, OFFER, SOCIAL, NOTIFICATIONS, FORUM
}
